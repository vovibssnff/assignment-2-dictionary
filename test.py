from subprocess import Popen, PIPE

inputs = ["Man", "Woman", "On_my_god", "__assembly__", ""]
outputs = ["Content", "Oh_my_god", "", "", ""]
errors = ["", "", "Entry not found", "Entry not found", "Input error"]

for i in range(len(inputs)):
    p = Popen("./lab_2", shell=False, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    inp = inputs[i]
    out = outputs[i]
    err = errors[i]
    data = p.communicate((inp + "\n").encode())  # Encode the input as bytes
    out_data, err_data = data
    out_data = out_data.decode()  # Decode the output data to a string
    err_data = err_data.decode()  # Decode the error data to a string
    if out_data.strip() == out and err_data.strip() == err:
        print(f"Passed tests: \"{inp}\"")
    else:
        print(f"Failed tests: \"{out_data}\", stderr: \"{err_data}\". Expected output: \"{out}\", stderr: \"{err}\"")
