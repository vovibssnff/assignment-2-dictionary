%include "lib.inc"

global find_word

section .text

; размер указателя на некст элемент
%define PNT 8

; rdi - pointer to value string
; rsi - pointer to the first entry
; rax - returns addres of entry, 0 if not found
; проходит по всему словарю, сравнивает rdi 
find_word:
        push r12
        mov r12, rsi                                    ; текущий адрес энтри
        .loop:
                test r12, r12
                jz .nope
                lea rsi, [r12+PNT]                      ; пишем указатель на строку
                call string_equals
                test rax, rax
                jnz .yep

                mov r12, [r12]

                jmp .loop

        .yep:
                mov rax, r12
                pop r12
                ret

        .nope:
                xor rax, rax
                pop r12
                ret
        